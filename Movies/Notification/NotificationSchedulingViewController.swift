//
//  NotificationSchedulingViewController.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class NotificationSchedulingViewController: UIViewController, MovieConsumer {
    private var movie: Movie?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateTimePicker: UIDatePicker!
    
    func consume(_ movie: Movie) {
        self.movie = movie
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Reminder for \(movie?.title ?? "this movie")"
    }
    
    @IBAction func didTouchCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTouchCreate(_ sender: UIButton) {
        NotificationManager.schedule(NotificationManager.SimpleNotification(title: "Movie reminder", subtitle: movie?.title ?? "", body: movie?.synopsis ?? "", date: dateTimePicker.date))
        dismiss(animated: true, completion: nil)
    }
}
