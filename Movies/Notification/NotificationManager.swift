//
//  NotificationManager.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import Foundation
import UserNotifications

class NotificationManager {
    struct SimpleNotification {
        let title: String
        let subtitle: String
        let body: String
        let date: Date
    }
    
    private static let notificationCenter = UNUserNotificationCenter.current()
    
    static func schedule(_ notification: SimpleNotification) {
        notificationCenter.getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .authorized:
                finishScheduling(notification)
                break
            default:
                requestPermissionToSchedule(notification)
                break
            }
        }
    }
    
    private static func finishScheduling(_ notification: SimpleNotification) {
        let id = String(Date().timeIntervalSince1970)
        let content = UNMutableNotificationContent()
        content.title = notification.title
        content.subtitle = notification.subtitle
        content.body = notification.body
        
        content.sound = UNNotificationSound(named: UNNotificationSoundName.init("sound.mp3"))
        content.categoryIdentifier = "lembrete"
        
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute],
                                                             from: notification.date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        
        notificationCenter.add(request) { error in
            print(error ?? "Success on notification scheduling")
        }
    }
    
    private static func requestPermissionToSchedule(_ notification: SimpleNotification) {
        let confirmAction = UNNotificationAction(identifier: "confirm", title: "Ok", options: [.foreground])
        let cancelAction = UNNotificationAction(identifier: "cancel", title: "Cancel", options: [])
        let category = UNNotificationCategory(identifier: "reminder", actions: [confirmAction, cancelAction], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "", options: [.allowInCarPlay, .customDismissAction])
        notificationCenter.setNotificationCategories([category])
        
        notificationCenter.requestAuthorization(options: [.alert, .badge, .sound, .carPlay]) { _, _ in
            schedule(notification)
        }
    }
}
