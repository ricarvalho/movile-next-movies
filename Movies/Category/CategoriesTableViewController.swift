//
//  CategoriesTableViewController.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class CategoriesTableViewController: UITableViewController, MovieConsumer {
    private var movie: Movie?
    private var categories: [Category] = CategoryModelController.allCategories.sorted(by: { $0.title ?? "" < $1.title ?? "" })
    @IBOutlet var emptyView: UIView!
    
    func consume(_ movie: Movie) {
        self.movie = movie
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.backgroundView = categories.count == 0 ? emptyView : nil
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath)
        let category = categories[indexPath.row]
        cell.textLabel?.text = category.title
        cell.accessoryType = movie?.categories?.contains(category) ?? false ? .checkmark : .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard var movieCategories = movie?.categories as? Set<Category> else { return }
        
        let category = categories[indexPath.row]
        if movieCategories.contains(category) {
            movieCategories.remove(category)
        } else {
            movieCategories.insert(category)
        }
        movie?.categories = movieCategories as NSSet
        
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        cell.accessoryType = movieCategories.contains(category) ? .checkmark : .none
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let category = categories.remove(at: indexPath.row)
            PersistenceManager.defaultContext.delete(category)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.backgroundView = categories.count == 0 ? emptyView : nil
        }
    }
    
    @IBAction func didTouchAdd(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Add Category", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] (_) in
            guard let title = alert.textFields?.first?.text, title.count > 0 else { return }
            guard self?.categories.first(where: { $0.title == title }) == nil else { return }
            
            CategoryModelController.createCategory().title = title
            self?.categories = CategoryModelController.allCategories.sorted(by: { $0.title ?? "" < $1.title ?? "" })
            self?.tableView.backgroundView = self?.categories.count ?? 0 == 0 ? self?.emptyView : nil
            self?.tableView.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addTextField { titleTextField in
            titleTextField.placeholder = "Title"
        }
        present(alert, animated: true, completion: nil)
    }
}
