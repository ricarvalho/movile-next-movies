//
//  CategoryModelController.swift
//  Movies
//
//  Created by Ricardo Carvalho on 24/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import Foundation
import CoreData

struct CategoryModelController {
    static var allCategories: [Category] {
        return (try? PersistenceManager.defaultContext.fetch(Category.fetchRequest())) ?? []
    }
    
    static func createCategory() -> Category {
        return Category(context: PersistenceManager.defaultContext)
    }
}
