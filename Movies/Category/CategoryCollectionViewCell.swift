//
//  CategoryCollectionViewCell.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell, StringConsumer {
    @IBOutlet weak var titleLabel: UILabel!
    
    func consume(_ string: String) {
        titleLabel.text = string
    }
}
