//
//  MovieConsumer.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import Foundation

protocol MovieConsumer {
    func consume(_ movie: Movie)
}
