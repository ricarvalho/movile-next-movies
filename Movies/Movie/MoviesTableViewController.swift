//
//  MoviesTableViewController.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class MoviesTableViewController: UITableViewController {
    private var movies: [Movie] = MovieModelController.allMovies
    @IBOutlet var emptyView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        movies = MovieModelController.allMovies
        tableView.backgroundView = movies.count == 0 ? emptyView : nil
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath)
        let movie = movies[indexPath.row]
        if let cell = cell as? MovieConsumer {
            cell.consume(movie)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let movie = movies.remove(at: indexPath.row)
            PersistenceManager.defaultContext.delete(movie)
            PersistenceManager.saveDefaultContext()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.backgroundView = movies.count == 0 ? emptyView : nil
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let _ = sender as? UITableViewCell else { return }
        guard let destination = segue.destination as? MovieConsumer else { return }
        guard let indexPath = tableView.indexPathForSelectedRow else { return }
        let movie = movies[indexPath.row]
        destination.consume(movie)
    }
}
