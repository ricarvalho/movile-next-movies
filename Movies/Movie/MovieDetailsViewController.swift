//
//  MovieDetailsViewController.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController, MovieConsumer {
    private var movie: Movie?
    private var keyboardManager: KeyboardManager?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var synopsisLabel: UILabel!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    
    func consume(_ movie: Movie) {
        self.movie = movie
    }
    
    override func viewDidLoad() {
        keyboardManager = KeyboardManager.init(toManageKeyboardOn: scrollView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateViews()
    }
    
    private func updateViews() {
        titleLabel.text = movie?.title
        ratingLabel.text = movie?.formattedRating
        durationLabel.text = movie?.formattedDuration
        synopsisLabel.text = movie?.synopsis
        
        if let image = movie?.image {
            posterImageView.image = UIImage(data: image)
        }
        
        categoriesCollectionView.reloadData()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let movie = movie else { return }
        guard let destination = segue.destination as? MovieConsumer else { return }
        destination.consume(movie)
    }
}

extension MovieDetailsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movie?.categories?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath)
        
        guard var categories = movie?.categories?.allObjects as? [Category] else { return cell }
        categories.sort(by: { $0.title ?? "" < $1.title ?? "" })
        
        let category = categories[indexPath.item]
        if let cell = cell as? StringConsumer {
            cell.consume(category.title ?? "")
        }
        return cell
    }
}
