//
//  MovieTableViewCell.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell, MovieConsumer {
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    func consume(_ movie: Movie) {
        titleLabel.text = movie.title
        ratingLabel.text = movie.formattedRating
        durationLabel.text = movie.formattedDuration
        
        if let image = movie.image {
            posterImageView.image = UIImage(data: image)
        }
        posterImageView.isHidden = posterImageView.image == nil
    }
}
