//
//  MovieModelController.swift
//  Movies
//
//  Created by Ricardo Carvalho on 24/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import Foundation
import CoreData

struct MovieModelController {
    static var allMovies: [Movie] {
        return (try? PersistenceManager.defaultContext.fetch(Movie.fetchRequest())) ?? []
    }
    
    static func createMovie() -> Movie {
        return Movie(context: PersistenceManager.defaultContext)
    }
}
