//
//  MovieEditingViewController.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class MovieEditingViewController: UIViewController, MovieConsumer {
    private static let scaledPosterImageGreaterDimension: CGFloat = 800
    private var movie: Movie?
    private var keyboardManager: KeyboardManager?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var durationTextField: UITextField!
    @IBOutlet weak var durationTimePicker: UIDatePicker!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingSlider: UISlider!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var synopsisTextView: UITextView!
    
    func consume(_ movie: Movie) {
        self.movie = movie
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardManager = KeyboardManager.init(toManageKeyboardOn: scrollView)
        
        if movie == nil {
            movie = MovieModelController.createMovie()
        }
        updateViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        categoriesCollectionView.reloadData()
    }
    
    private func updateViews() {
        titleTextField.text = movie?.title
        durationTextField.text = movie?.formattedDuration
        durationTimePicker.countDownDuration = movie?.duration ?? 0
        ratingLabel.text = movie?.formattedRating
        ratingSlider.value = movie?.rating ?? 0
        synopsisTextView.text = movie?.synopsis
        
        if let image = movie?.image {
            posterImageView.image = UIImage(data: image)
        }
        posterImageView.isHidden = posterImageView.image == nil
        
        categoriesCollectionView.reloadData()
    }
    
    @IBAction func didFinishInput() {
        view.endEditing(true)
    }
    
    @IBAction func durationDidChangeValue(_ sender: UIDatePicker) {
        movie?.duration = sender.countDownDuration
        durationTextField.text = movie?.formattedDuration
    }
    
    @IBAction func ratingDidChangeValue(_ sender: UISlider) {
        movie?.rating = sender.value
        ratingLabel.text = movie?.formattedRating
    }
    
    @IBAction func didTouchCancelEdition() {
        PersistenceManager.rollbackDefaultContext()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTouchSave() {
        updateMovieData()
        PersistenceManager.saveDefaultContext()
        navigationController?.popViewController(animated: true)
    }
    
    private func updateMovieData() {
        movie?.title = titleTextField.text
        movie?.duration = durationTimePicker.countDownDuration
        movie?.rating = ratingSlider.value
        movie?.synopsis = synopsisTextView.text
        if let imageData = posterImageView.image?.pngData() {
            movie?.image = imageData
        }
    }
    
    @IBAction func didTouchPickImage() {
        let alert = UIAlertController(title: "Poster Image", message: "Choose from where to pick image", preferredStyle: .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            alert.addAction(UIAlertAction(title: "Camera", style: .default) { _ in
                self.presentImagePicker(withSourceType: .camera)
            })
        }
        
        alert.addAction(UIAlertAction(title: "Photos Album", style: .default) { _ in
            self.presentImagePicker(withSourceType: .savedPhotosAlbum)
        })
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    private func presentImagePicker(withSourceType type: UIImagePickerController.SourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = type
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let movie = movie else { return }
        guard let destination = segue.destination as? MovieConsumer else { return }
        destination.consume(movie)
    }
}

extension MovieEditingViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (movie?.categories?.count ?? 0) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath)
        
        guard indexPath.item > 0 else {
            if let cell = cell as? StringConsumer {
                cell.consume("+")
            }
            return cell
        }
        
        guard var categories = movie?.categories?.allObjects as? [Category] else { return cell }
        categories.sort(by: { $0.title ?? "" < $1.title ?? "" })
        
        let category = categories[indexPath.item - 1]
        if let cell = cell as? StringConsumer {
            cell.consume(category.title ?? "")
        }
        return cell
    }
}

extension MovieEditingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage ?? info[.originalImage] as? UIImage else {
            dismiss(animated: true, completion: nil)
            return
        }
        posterImageView.image = image.scaled(withGreaterDimension: MovieEditingViewController.scaledPosterImageGreaterDimension)
        posterImageView.isHidden = posterImageView.image == nil
        
        dismiss(animated: true, completion: nil)
    }
}
