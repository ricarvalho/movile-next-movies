//
//  IntrinsicContentSizeCollectionView.swift
//  Movies
//
//  Created by Ricardo Carvalho on 24/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class IntrinsicContentSizeCollectionView: UICollectionView {
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return contentSize
        }
    }
}
