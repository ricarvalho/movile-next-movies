//
//  UITextView+InputView.swift
//  Movies
//
//  Created by Ricardo Carvalho on 25/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

extension UITextView {
    @IBOutlet var customInputView: UIView? {
        get {
            return inputView
        }
        
        set {
            inputView = newValue
        }
    }
    
    @IBOutlet var customInputAccessoryView: UIView? {
        get {
            return inputAccessoryView
        }
        
        set {
            inputAccessoryView = newValue
        }
    }
}
