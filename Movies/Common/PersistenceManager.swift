//
//  PersistenceManager.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit
import CoreData

struct PersistenceManager {
    private static var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    static var defaultContext: NSManagedObjectContext {
        return appDelegate.persistentContainer.viewContext
    }
    
    static func saveDefaultContext() {
        if defaultContext.hasChanges {
            do {
                try defaultContext.save()
            } catch {
                print("Error while saving ManagedObjectContext")
            }
        }
    }
    
    static func rollbackDefaultContext() {
        if defaultContext.hasChanges {
            defaultContext.rollback()
        }
    }
}
