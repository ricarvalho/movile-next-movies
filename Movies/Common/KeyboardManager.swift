//
//  KeyboardManager.swift
//  Movies
//
//  Created by Ricardo Carvalho on 17/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

class KeyboardManager {
    private weak var scrollView: UIScrollView?
    
    init(toManageKeyboardOn scrollView: UIScrollView) {
        self.scrollView = scrollView
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo, let scrollView = scrollView else { return }
        let height = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.size.height ?? 0
        
        scrollView.contentInset.bottom = height
        scrollView.scrollIndicatorInsets.bottom = height
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        guard let scrollView = scrollView else { return }
        scrollView.contentInset.bottom = 0
        scrollView.scrollIndicatorInsets.bottom = 0
    }
}
