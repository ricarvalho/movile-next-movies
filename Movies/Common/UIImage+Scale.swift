//
//  UIImage+Scale.swift
//  Movies
//
//  Created by Ricardo Carvalho on 24/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

extension UIImage {
    private var greaterDimension: CGFloat {
        return max(size.width, size.height)
    }
    
    private var smallerDimension: CGFloat {
        return min(size.width, size.height)
    }
    
    func scaled(withGreaterDimension dimension: CGFloat) -> UIImage {
        let scaleFactor = dimension / greaterDimension
        let scaledSize = size.applying(CGAffineTransform(scaleX: scaleFactor, y: scaleFactor))
        return self.scaled(toSize: scaledSize)
    }
    
    func scaled(withSmallerDimension dimension: CGFloat) -> UIImage {
        let scaleFactor = dimension / smallerDimension
        let scaledSize = size.applying(CGAffineTransform(scaleX: scaleFactor, y: scaleFactor))
        return self.scaled(toSize: scaledSize)
    }
    
    private func scaled(toSize scaledSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(scaledSize)
        draw(in: CGRect(origin: CGPoint.zero, size: scaledSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage ?? self
    }
}
