//
//  UIImageScaleTests.swift
//  MoviesTests
//
//  Created by Ricardo Carvalho on 15/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import XCTest
@testable import Movies

class UIImageScaleTests: XCTestCase {
    
    func image(with size: CGSize) -> UIImage? {
        let rect = CGRect(origin: CGPoint.zero, size: size)
        UIGraphicsBeginImageContextWithOptions(size, true, 1)
        UIColor.white.set()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    // MARK: - Special Cases
    
    func testScaleToZero() {
        let size = CGSize(width: 10, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 0)
        XCTAssertEqual(scaledImage?.size, size) // Invalid size, returns same image
        
        scaledImage = image?.scaled(withSmallerDimension: 0)
        XCTAssertEqual(scaledImage?.size, size) // Invalid size, returns same image
    }
    
    func testScaleToNearZero() {
        let size = CGSize(width: 10, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 0.1)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 1, height: 1)) // Minimum image size returned from UIGraphicsImageContext
        
        scaledImage = image?.scaled(withSmallerDimension: 0.1)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 1, height: 1)) // Minimum image size returned from UIGraphicsImageContext
    }
    
    // MARK: - Square
    
    func testScaleToOne() {
        let size = CGSize(width: 10, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 1)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 1, height: 1))
        
        scaledImage = image?.scaled(withSmallerDimension: 1)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 1, height: 1))
    }
    
    func testScaleToHalf() {
        let size = CGSize(width: 10, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 5)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 5, height: 5))
        
        scaledImage = image?.scaled(withSmallerDimension: 5)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 5, height: 5))
    }
    
    func testScaleToSame() {
        let size = CGSize(width: 10, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 10)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 10, height: 10))
        
        scaledImage = image?.scaled(withSmallerDimension: 10)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 10, height: 10))
    }
    
    func testScaleToDouble() {
        let size = CGSize(width: 10, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 20)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 20, height: 20))
        
        scaledImage = image?.scaled(withSmallerDimension: 20)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 20, height: 20))
    }
    
    // MARK: - Horizontal
    
    func testScaleToOneHorizontal() {
        let size = CGSize(width: 20, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 1)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 1, height: 1)) // Minimum image size returned from UIGraphicsImageContext
        
        scaledImage = image?.scaled(withSmallerDimension: 1)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 2, height: 1))
    }
    
    func testScaleToHalfHorizontal() {
        let size = CGSize(width: 20, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 10)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 10, height: 5))
        
        scaledImage = image?.scaled(withSmallerDimension: 5)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 10, height: 5))
    }
    
    func testScaleToSameHorizontal() {
        let size = CGSize(width: 20, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 20)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 20, height: 10))
        
        scaledImage = image?.scaled(withSmallerDimension: 10)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 20, height: 10))
    }
    
    func testScaleToDoubleHorizontal() {
        let size = CGSize(width: 20, height: 10)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 40)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 40, height: 20))
        
        scaledImage = image?.scaled(withSmallerDimension: 20)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 40, height: 20))
    }
    
    // MARK: - Vertical
    
    func testScaleToOneVertical() {
        let size = CGSize(width: 10, height: 20)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 1)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 1, height: 1)) // Minimum image size returned from UIGraphicsImageContext
        
        scaledImage = image?.scaled(withSmallerDimension: 1)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 1, height: 2))
    }
    
    func testScaleToHalfVertical() {
        let size = CGSize(width: 10, height: 20)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 10)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 5, height: 10))
        
        scaledImage = image?.scaled(withSmallerDimension: 5)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 5, height: 10))
    }
    
    func testScaleToSameVertical() {
        let size = CGSize(width: 10, height: 20)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 20)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 10, height: 20))
        
        scaledImage = image?.scaled(withSmallerDimension: 10)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 10, height: 20))
    }
    
    func testScaleToDoubleVertical() {
        let size = CGSize(width: 10, height: 20)
        let image = self.image(with: size)
        var scaledImage = image?.scaled(withGreaterDimension: 40)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 20, height: 40))
        
        scaledImage = image?.scaled(withSmallerDimension: 20)
        XCTAssertEqual(scaledImage?.size, CGSize(width: 20, height: 40))
    }
}
